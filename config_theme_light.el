;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Maxime Bombar"
      user-mail-address "bombar@crans.org")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
(setq doom-theme 'doom-one-light)

;; or for treemacs users
(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
(doom-themes-treemacs-config)

;; bind F5 to toggle treemacs
(global-set-key [f5] 'treemacs)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; Show trailing whitespaces
(setq show-trailing-whitespace t)

(setq doom-localleader-key ",")

(setq-default fill-column 70)

(remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.


;; ispell configuration. Needs packages aspell aspell-fr aspell-en
(setq ispell-dictionary "british")


;; Define the list of languages
(let ((langs '("british" "francais")))
  (setq lang-ring (make-ring (length langs)))
  (dolist (elem langs) (ring-insert lang-ring elem)))


;; Function to cycle through ispell languages
(defun cycle-ispell-languages ()
  (interactive)
  (let ((lang (ring-ref lang-ring -1)))
    (ring-insert lang-ring lang)
    (ispell-change-dictionary lang)))

;; bind F6 key to cycle through ispell
(global-set-key [f6] 'cycle-ispell-languages)

;; bind F4 to check the language
(global-set-key [f4] 'ispell)


(use-package! lsp
  :init
  ;; configure lsp for python. See https://github.com/emacs-lsp/lsp-mode/blob/master/lsp-pyls.el
  (setq lsp-pyls-plugins-pylint-enabled t)
  (setq lsp-pyls-plugins-yapf-enabled t)
  (setq lsp-pyls-plugins-flake8-enabled t)
  (setq lsp-pyls-plugins-pycodestyle-enabled t))


;; Automatically load lsp in python mode
(add-hook! 'python-mode-hook 'lsp)

;; bind C-x C-g to format-all-buffer
(map! "C-x C-g" #'format-all-buffer)

;; bind C-c C-y to yasnippet map
(map! "C-c C-y" #'yas/describe-tables)

(use-package! magma-mode
  :mode "\\.magma\\'"
  :mode "\\.mgm\\'"
  :mode "\\.m\\'"

  :bind (:map magma-mode-map
         ("RET" . magma-insert-newline)
         ("C-RET" . magma-insert-special-newline)
         ("TAB" . completion-at-point))

  :config
  (require 'magma-snippets))

(use-package! latex
  :init
  (setq TeX-parse-all-errors t)
  (setq TeX-show-compilation t)
  ;; (setq +latex-viewers '(okular)))
  (setq +latex-viewers '(evince)))

(add-hook 'LaTeX-mode-hook
          (lambda () (set (make-local-variable 'TeX-electric-math)
                          (cons "\\(" "\\)"))))





(use-package! sage-shell-mode
  :mode "\\.sage\\'"
  :init
  (setq sage-shell:input-history-cache-file "~/.emacs.d/.sage_shell_input_history")
  (setq sage-shell:sage-root "~/Sage/sage")
  (custom-set-variables
   '(sage-shell:use-prompt-toolkit nil)
   '(sage-shell:use-simple-prompt t)
   '(sage-shell:set-ipython-version-on-startup t)
   '(sage-shell:check-ipython-version-on-startup t)))
  






;; (use-package! sage-shell-mode
;;   ;; :mode "\\.sage\\'"
;;   :init
;;   ;; configure sage-shell-mode
;;   (setq sage-shell:input-history-cache-file "~/.emacs.d/.sage_shell_input_history")
;;   (custom-set-variables
;;       '(sage-shell:use-prompt-toolkit nil)
;;       '(sage-shell:use-simple-prompt t)
;;       '(sage-shell:set-ipython-version-on-startup nil)
;;       '(sage-shell:check-ipython-version-on-startup nil)
;;       '(sage-shell:define-alias)))


;; (after! sage-shell-mode
;;   (add-hook 'sage-shell-mode-hook #'eldoc-mode)
;;   (add-hook 'sage-shell:sage-mode-hook #'eldoc-mode)
;;   (add-hook 'sage-shell:sage-mode-hook 'ac-sage-setup)
;;   (add-hook 'sage-shell-mode-hook 'ac-sage-setup))

;; (map! :after sage-shell-mode
;;       :map sage-shell-mode-map
;;       "C-c C-i" 'helm-sage-complete
;;       "C-c C-h" 'helm-sage-describe-object-at-point
;;       "M-r" 'heml-sage-command-history
;;       "C-c o" 'helm-sage-output-history)




  





;;; https://emacs.stackexchange.com/questions/55367/how-to-render-html-in-web-mode
(with-eval-after-load 'web-mode
  (define-key web-mode-map (kbd "C-c C-a") 'browse-url-of-buffer))


;; For GMP
;; See https://gmplib.org/gmp-man-6.2.1.pdf p34
;;

(with-eval-after-load "info-lookup-symbol"
 ’(let ((mode-value (assoc ’c-mode (assoc ’symbol info-lookup-alist))))
   (setcar (nthcdr 3 mode-value)
    (cons ’("(gmp)Function Index" nil "^ -.* " "\\>")
     (nth 3 mode-value)))))
